from django.shortcuts import render
from django.http import HttpResponse
from .models import Entry
from .forms import KegiatanForm

# Create your views here.
def base(request):
	return render(request, 'base.html', {})

def landing(request):
	return render(request, 'landing.html', {})

def home(request):
	return render(request, 'home.html', {})

def aboutme(request):
	return render(request, 'aboutme.html', {})

def skills(request):
	return render(request, 'skills.html', {})

def gallery(request):
	return render(request, 'gallery.html', {})

def addSchedule(request):
    if(request.method == 'POST'):
    	form = KegiatanForm(request.POST)
    	if(form.is_valid()):
    		jadwal = Entry(
    			category = form.cleaned_data['kategori'],
    			event = form.cleaned_data['kegiatan'],
    			location = form.cleaned_data['lokasi'],
    			date = form.cleaned_data['tanggal'],
    			time = form.cleaned_data['waktu'],
    		)
    		jadwal.save()

    form = KegiatanForm()
    context = {
    	'form' : form
    }
    return render(request, 'addSchedule.html', context)

def schedule(request): 
    if(request.method == 'POST'):
        form = KegiatanForm(request.POST)
        pk = request.POST['id']
        obj = Entry.objects.get(pk=pk)
        obj.delete()

    jadwal_objects = Entry.objects.all()
    jadwal_list = {
        'jadwal' : jadwal_objects
    }
    return render(request, 'schedule.html', jadwal_list)
