from django import forms
from .models import Entry

class KegiatanForm(forms.Form):
    pilihan = [ 
        ('Kuliah', 'Kuliah'),
        ('Event', 'Event'),
        ('Personal', 'Personal'),
    ]
    kategori = forms.ChoiceField(choices=pilihan, label='Kategori:')
    kegiatan = forms.CharField(label='Kegiatan:', max_length=30, 
        widget=forms.TextInput())
    lokasi = forms.CharField(label='Lokasi:', max_length=30, 
        widget=forms.TextInput())
    tanggal = forms.DateField(label='Tanggal (dd-mm-yyyy):',
        widget=forms.SelectDateWidget())
    waktu = forms.CharField(label='Waktu (hh-mm):', max_length=10, 
        widget=forms.TextInput())
    class Meta:
        model = Entry
        fields = [
            "category",
            "event",
            "location",
            "date",
            "time",
        ]


    # def __init__(self, *args, **kwargs):
    #     super(KegiatanForm, self).__init__(*args, **kwargs)
    #     for visible in self.visible_fields():
    #         visible.field.widget.attrs['class'] = 'form-control'
