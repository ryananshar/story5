from django.db import models

# Create your models here.
class Entry(models.Model):
	"""docstring for Entry"""
	category = models.CharField(max_length = 1)
	event = models.CharField(max_length = 50)
	location = models.CharField(max_length = 50)
	date = models.DateField()
	time = models.CharField(max_length = 10)