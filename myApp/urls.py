from django.urls import include, path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('', views.base, name="base"),
    path('landing/', views.landing, name="landing"),
    path('home/', views.home, name="home"),
    path('skills/', views.skills, name="skills"),
    path('addSchedule/', views.addSchedule, name="addSchedule"),
    path('schedule/', views.schedule, name="schedule"),
]

urlpatterns += staticfiles_urlpatterns()
